#include <iostream>

using namespace std;

typedef struct TUzol
{
    int vek;
    string meno;
    TUzol *lavy, *pravy;
} uzol;

class strom
{
private:
    uzol *root;
public:
    strom()
    {
        root = NULL;
    }
    void vlozPrvok(int age, string name)
    {
        if(root == NULL)
        {
            root = new uzol();
            root->vek = age;
            root->meno = name;
            root->lavy = NULL;
            root->pravy = NULL;
            return;
        }
        else
        {
            insertUzol(age, name, root);
        }
    }

    void insertUzol(int age, string name, uzol *akt)
    {
        if(age < akt->vek)
        {
            if(akt->lavy == NULL)
            {
                akt->lavy = new uzol();
                akt->lavy->meno = name;
                akt->lavy->vek = age;
                akt->lavy->lavy = NULL;
                akt->lavy->pravy = NULL;
                return;

            }
            else
                insertUzol(age, name, akt->lavy);
        }
        else
        {
            if(akt->pravy == NULL)
            {
                akt->pravy = new uzol();
                akt->pravy->meno = name;
                akt->pravy->vek = age;
                akt->pravy->lavy = NULL;
                akt->pravy->pravy = NULL;
                return;

            }
            else
                insertUzol(age, name, akt->pravy);
        }
    }

    void vypis()
    {
        vypisPreorder(root);
        cout << endl;
    }

    void vypisPreorder(uzol *akt)
    {
        cout << akt->vek << " ";
        if(akt->lavy != NULL)
        {
            vypisPreorder(akt->lavy);
        }
        if(akt->pravy != NULL)
        {
            vypisPreorder(akt->pravy);
        }
    }

    void vypisMeno(int age) {
        uzol *najdi;
        najdi = najdiUzol(age, root);
        cout << najdi->meno << endl;
    }

    uzol *najdiUzol(int age, uzol *akt) {
        if(age == akt->vek) {
            return akt;
        }
        if(age < akt->vek) {
            return najdiUzol(age, akt->lavy);
        }
        if(age > akt->vek) {
            return najdiUzol(age, akt->pravy);
        }
        return NULL;
    }
};

int main()
{
    strom st;
    st.vlozPrvok(10, "Jozo");
    st.vlozPrvok(5, "Fero");
    st.vlozPrvok(12, "Fero");
    st.vlozPrvok(1, "Fero");
    st.vlozPrvok(0, "Fero");
    st.vlozPrvok(4, "Dano");
    st.vlozPrvok(7, "Fero");
    st.vlozPrvok(6, "Fero");
    st.vlozPrvok(9, "Fero");

    st.vypis();

    st.vypisMeno(4);
    return 0;
}
